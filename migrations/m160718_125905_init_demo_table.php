<?php

use yii\db\Migration;

class m160718_125905_init_demo_table extends Migration
{
    public function up()
    {
		$this->createTable(
			'demo',
			[
				'id' => 'pk',
				'name'=> 'string',
				'demo_date' => 'date',
				'notes' => 'text',
			],
			'ENGINE=InnoDB'
		);
    }

    public function down()
    {
        $this->dropTable('demo');
    }

   
}
